# [](#authors)Authors

##  Scrum Master

- Sam Foulon

## Développeurs

- Loris Danel
- Matis Cartier
- Jonathan Obin
-  Sam Foulon

## Tuteurs

- Minh-Tan Pham
- Nicolas Le Sommer 

## Contributeurs

- Groupe [Application Web](https://gitlab.com/gmva_sspc/web_app)
-  Groupe [ Prévision Météorologiques](https://gitlab.com/gmva_sspc/meteo_prev)
-  Groupe [Visualisation des données](https://gitlab.com/gmva_sspc/dataviz)
