# Data Client

Data given by the client. Our scripts were used to get the data from mails, put them in the right format and insert the data in InfluxDB.

## Table of contents

- [Schema](#Schema)
- [Installation](#installation)
- [Documentation](#Documentation)
- [Test](#Test)

## Schema

Explanatory diagram of the project

Shows the link between different parts of our project.

![](Documentation/Image/Readme/Schema_pour_S4.png)

## Installation

To execute the scripts in a docker, refer to the following steps :

1. Create a docker image

```
docker build -t chauffage .
```

2. Run the docker image


Required arguments:

| Parameters                 | Description   |	
| :------------------------ | :-------------|
| `-e MAIL=' '` | Mail Adress which receive the reports|
| `-e MAIL_PASSWORD=' '` |  Application Password (see [Configuration_Gmail_Python](Documentation/Configuration_Gmail_Python.pdf))|
| `-e MAIL_SENDER=' '` | Adress which sends the reports|
|`-e TOKEN_INFLUXDB=' '` | Name of the organization|
| `-e ORGANIZATION_INFLUXDB=` | Database token|

Command example :

```
docker run -d --name Chauffage -e MAIL='dataSSPC56@gmail.com -e MAIL_PASSWORD='password' -e MAIL_SENDER='rapport_mail@pcwin.com' -e TOKEN_INFLUXDB='token' -e ORGANIZATION_INFLUXDB='orga' chauffage
```

This tutorial is only to install the scripts in docker. 
To install the project, go [here](/)


## Documentation

Guides created to make it easier to follow the steps necessary so that our scripts works.
These guides concern the use of PcWin2 and how to link the mailbox so that with Python we can get the attachment of a mail and then download it.

## Test

Scripts which are used to get the attachment from a mail, put it to the right format and insert the data into the database.