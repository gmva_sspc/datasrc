# Test

This part contains the tests scripts used to check if they work (get the data of a mail, put it in the right format and then insert it into InfluxDB).

## Table of contents

- [Insertion](#insertion)
- [Mail](#mail)
- [Main](#main)

## Insertion

Script which inserts the data into InfluxDB. The data has to respect a certain format for it to be inserted successfully in InfluxDB.

## Mail

Script which get the attachment of a mail, add it in a folder and use the insertion script to insert the data.
The file is then deleted.

## Main

Script which contains both the Mail and the Insertion. This script is the final version of both scripts.

