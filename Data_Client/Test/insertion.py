from influxdb_client import InfluxDBClient
import pandas as pd
import time
import os

URL = "https://sspc.fbyx.fr/"
ORGA = "SSPC"
TOKEN = "The_token"
MeasurementName = "data"
tag = "attributs"
timestamp = "date" # default value

def Insertion(data, BUCKET):
    print("Insertion des données...")
    i = 0
    increment = 1000
    client = InfluxDBClient(url=URL,
                            token=TOKEN,
                            org=ORGA)

    if len(data) < increment:
        increment = len(data)
    i2 = increment

    for k in range(0, len(data), increment):

        df_cut = data.iloc[i:i2, :]
        with client.write_api() as write_client:
            write_client.write(bucket=BUCKET, record=df_cut, data_frame_measurement_name=MeasurementName,
                               data_frame_tag_columns=[tag], data_frame_timestamp_column=timestamp)
        i2 = i
        i += increment
        print("Nombre de données inserées ", i, " soit ", round(i / len(data) * 100, 2), "%") # with pourcentage
        time.sleep(0.05) # limit 20 request per second

def create_bucket(bucket_name):
    client = InfluxDBClient(url=URL,
                            token=TOKEN,
                            org=ORGA)
    try:
        client.buckets_api().create_bucket(bucket_name=bucket_name)
        print("Bucket créé")
    except:
        print("Bucket déjà existant")


def format_data(path):

    print("Formatage des données...")

    data = pd.read_excel(path, sheet_name="DATA", engine="openpyxl")

    #formatage date
    data['date'] = pd.to_datetime(data['date'], format='%Y-%m-%d %H:%M:%S')

    #formatage attributs
    data['rank'] = data['rank'].str[3:]

    #inversion colonne rank et value
    col_list = list(data.columns)
    value, rank = col_list.index("value"), col_list.index("rank")
    col_list[rank], col_list[value] = col_list[value], col_list[rank]
    data = data[col_list]

    #set la date comme index
    data.set_index(['date'])

    #renommer colonne rank en attributs
    data = data.rename(columns={'rank': 'attributs'})

    return data
    #Insertion(data, "test_s4")

