import imaplib
import email
from email.header import decode_header
import webbrowser
import os

import insertion


def process_mailbox():
    #Information sur la boite mail
    username = os.getenv("MAIL")
    password = os.getenv("MAIL_PASSWORD")

    # Information sur l'envoyeur
    sender_email = os.getenv("MAIL_SENDER")

    # Ceation d'un repertoir ou seron stocker les data
    detach_dir = '.'
    if 'pieces_jointes' not in os.listdir(detach_dir):
        os.mkdir('pieces_jointes')

    #Connexion au serveur mail
    imap = imaplib.IMAP4_SSL("imap.gmail.com")
    imap.login(username, password)
    status, messages = imap.select("Inbox")
    #Nombre de mail que l'on va regarder = nombre de rapport
    N = 1
    messages = int(messages[0])

    # Filter emails from specific sender
    sender_query = f'(FROM "{sender_email}")'
    status, msgs = imap.search(None, sender_query)

    for i in msgs[0].split()[::-1][:N]:
        #Lecture de la boite  mail
        res, msg = imap.fetch(i, "(RFC822)")
        for response in msg:
            #Recherche du messages
            if isinstance(response, tuple):
                msg = email.message_from_bytes(response[1])
                subject = decode_header(msg["Subject"])[0][0]
                if isinstance(subject, bytes):
                    subject = subject.decode()
                if msg.is_multipart():
                    for part in msg.walk():
                        content_type = part.get_content_type()
                        content_disposition = str(part.get("Content-Disposition"))
                        try:
                            body = part.get_payload(decode=True).decode()
                        except:
                            pass
                        if content_type == "text/plain" and "attachment" not in content_disposition:
                            #print(body)
                            pass
                        elif "attachment" in content_disposition:
                            print("Mail avec pièce jointe trouvée")
                            filename = part.get_filename()
                            if filename:
                                filepath = os.path.join(detach_dir, 'pieces_jointes', filename)
                                open(filepath, "wb").write(part.get_payload(decode=True))

                                #formatage des données
                                data = insertion.format_data(filepath)

                                #creation du bucket
                                filename = filename.split(".")[0]
                                insertion.create_bucket(filename)

                                #insertion des données
                                insertion.Insertion(data, filename)

                                os.remove(filepath)
                                imap.store(i, '+FLAGS', '\\Deleted')
                                imap.expunge()
                                print("Mail supprimé")
                                print("Fichier supprimé")
                else:
                    content_type = msg.get_content_type()
                    body = msg.get_payload(decode=True).decode()
                    if content_type == "text/plain":
                        print(body)
                if content_type == "text/html":
                    if not os.path.isdir(subject):
                        os.mkdir(subject)
                    filename = f"{subject[:50]}.html"
                    filepath = os.path.join(subject, filename)
                    open(filepath, "w").write(body)
                    webbrowser.open(filepath)
                print("fin du traitement")
    imap.close()
    imap.logout()
