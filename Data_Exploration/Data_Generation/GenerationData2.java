import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static jdk.nashorn.internal.objects.NativeMath.round;

public class GenerationData2 {
    public static void main(String[] args) {
        try {
            FileWriter csvWriter = new FileWriter("GenerationData2.csv");

            // Ajout des titres de colonnes
            csvWriter.append("Date");
            csvWriter.append(",");
            csvWriter.append("TemperatureEauBassinExterieurGrand");
            csvWriter.append(",");
            csvWriter.append("TemperatureEauBassinExterieurPetit");
            csvWriter.append(",");
            csvWriter.append("TemperatureEauBassinIntérieur");
            csvWriter.append(",");

            csvWriter.append("VitessePompeFiltrationBassinExterieurGrand");
            csvWriter.append(",");
            csvWriter.append("VitessePompeFiltrationBassinExterieurPetit");
            csvWriter.append(",");
            csvWriter.append("VitessePompeFiltrationBassinIntérieur");
            csvWriter.append(",");

            csvWriter.append("DuréeFonctionnementPompeExterieurGrand");
            csvWriter.append(",");
            csvWriter.append("DuréeFonctionnementPompeBassinExterieurPetit");
            csvWriter.append(",");
            csvWriter.append("DuréeFonctionnementPompeBassinIntérieur");
            csvWriter.append(",");

            csvWriter.append("TempératureAmbianteIntérieur");
            csvWriter.append(",");

            csvWriter.append("HumiditéAmbianteIntérieur");
            csvWriter.append(",");

            csvWriter.append("ConsommationElectriqueTotalePiscine");
            csvWriter.append(",");

            csvWriter.append("ConsommationElectriquePompeFiltration");
            csvWriter.append(",");
            csvWriter.append("ConsommationElectriqueÉclairage");
            csvWriter.append(",");
            csvWriter.append("ConsommationElectriqueChauffage");
            csvWriter.append(",");
            csvWriter.append("\n");

            // Ajout des données
            List<List<String>> rows = date();
            for (List<String> rowData : rows) {
                csvWriter.append(String.join(",", rowData));
                csvWriter.append("\n");
            }
            csvWriter.flush();
            csvWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<List<String>> date(){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        LocalDate startDate = LocalDate.of(2022, 1, 1);
        LocalDate endDate = LocalDate.of(2022, 12, 31);
        LocalDate currentDate = startDate;
        List<List<String>> rows = new ArrayList<List<String>>();
        while (currentDate.isBefore(endDate)) {
            String cos =CosEl(currentDate);
            String cosChauffage = CosCh(currentDate,cos);
            String cosF = CosCh(currentDate,cosChauffage);
            String cosE = String.valueOf(Double.parseDouble(cos) - (Double.parseDouble(cosChauffage) - Double.parseDouble(cosF)));
            rows.add(Arrays.asList(currentDate.format(formatter),tempBExGR(currentDate),tempBExPt(currentDate)
                    ,tempB(currentDate) ,VpompeExtG(currentDate),VpompeExtP(currentDate),VpompeI(currentDate)
                    ,DpompeExtG(currentDate),DpompeExtP(currentDate),"16",TempAmb(currentDate),Um(currentDate)
                    ,cos,cos,cosF,cosE,cosChauffage));
            currentDate = currentDate.plusDays(1);
        }
        return rows;
    }

    public static String tempBExGR(LocalDate currentDate) {
        List<List<String>> rows = new ArrayList<List<String>>();
        double tempD = 0;
        if (currentDate.getMonthValue() >= 5 && currentDate.getMonthValue() < 10) {
            if (currentDate.getMonthValue() == 6){
                tempD = RandomNumber(18,22);
            }else if (currentDate.getMonthValue() == 7 ||currentDate.getMonthValue() == 8){
                tempD = RandomNumber(23,29);
            }else {
                tempD = RandomNumber(20,25);
            }
        }
        tempD = Math.round(tempD * Math.pow(10, 3)) / Math.pow(10, 3);
        return String.valueOf(tempD);
    }

    public static String tempBExPt(LocalDate currentDate) {
        List<List<String>> rows = new ArrayList<List<String>>();
        double tempD = 0;
        if (currentDate.getMonthValue() >= 5 && currentDate.getMonthValue() < 10) {
            if (currentDate.getMonthValue() == 6){
                tempD = RandomNumber(19,23);
            }else if (currentDate.getMonthValue() == 7 ||currentDate.getMonthValue() == 8){
                tempD = RandomNumber(24,30);
            }else {
                tempD = RandomNumber(21,26);
            }
        }
        tempD = Math.round(tempD * Math.pow(10, 3)) / Math.pow(10, 3);
        return String.valueOf(tempD);
    }

    public static String tempB(LocalDate currentDate) {
        List<List<String>> rows = new ArrayList<List<String>>();
        double tempD = 0;
                tempD = RandomNumber(24,29);
        tempD = Math.round(tempD * Math.pow(10, 3)) / Math.pow(10, 3);
        return String.valueOf(tempD);
    }

    public static String VpompeExtG(LocalDate currentDate) {
        List<List<String>> rows = new ArrayList<List<String>>();
        double tempD = 0;
        if (currentDate.getMonthValue() >= 5 && currentDate.getMonthValue() < 10) {
            tempD = RandomNumber(78,81);
        }
        tempD = Math.round(tempD * Math.pow(10, 3)) / Math.pow(10, 3);
        return String.valueOf(tempD);
    }

    public static String VpompeExtP(LocalDate currentDate) {
        List<List<String>> rows = new ArrayList<List<String>>();
        double tempD = 0;
        if (currentDate.getMonthValue() >= 5 && currentDate.getMonthValue() < 10) {
            tempD = RandomNumber(38,41);
        }
        tempD = Math.round(tempD * Math.pow(10, 3)) / Math.pow(10, 3);
        return String.valueOf(tempD);
    }

    public static String VpompeI(LocalDate currentDate) {
        List<List<String>> rows = new ArrayList<List<String>>();
        double tempD = 0;
            tempD = RandomNumber(58,62);
        tempD = Math.round(tempD * Math.pow(10, 3)) / Math.pow(10, 3);
        return String.valueOf(tempD);
    }

    public static String DpompeExtG(LocalDate currentDate) {
        double tempD = 0;
        if (currentDate.getMonthValue() >= 5 && currentDate.getMonthValue() < 10) {
            tempD = 16;
        }
        tempD = Math.round(tempD * Math.pow(10, 3)) / Math.pow(10, 3);
        return String.valueOf(tempD);
    }

    public static String DpompeExtP(LocalDate currentDate) {
        double tempD = 0;
        if (currentDate.getMonthValue() >= 5 && currentDate.getMonthValue() < 10) {
            tempD = 14;
        }
        tempD = Math.round(tempD * Math.pow(10, 3)) / Math.pow(10, 3);
        return String.valueOf(tempD);
    }



    public static String TempAmb(LocalDate currentDate) {
        double tempD = RandomNumber(23,24);
        tempD = Math.round(tempD * Math.pow(10, 3)) / Math.pow(10, 3);
        return String.valueOf(tempD);
    }

    public static String Um(LocalDate currentDate) {
        double tempD = 0;
        if (currentDate.getMonthValue() >= 5 && currentDate.getMonthValue() < 10) {
            tempD = RandomNumber(40,45);
        }else if (currentDate.getMonthValue() >= 10 && currentDate.getMonthValue() <= 11) {
            tempD = RandomNumber(45,50);
        }else if (currentDate.getMonthValue() >11) {
            tempD = RandomNumber(50,55);
        }else if (currentDate.getMonthValue() <= 3) {
            tempD = RandomNumber(55,60);
        }else {
            tempD = RandomNumber(52,57);
        }
        tempD = Math.round(tempD * Math.pow(10, 3)) / Math.pow(10, 3);
        return String.valueOf(tempD);
    }

    public static String CosEl(LocalDate currentDate) {
        //12000
        double tempD = 0;
        if (currentDate.getMonthValue() >= 5 && currentDate.getMonthValue() < 10) {
            tempD = RandomNumber(11800,11200);
        }else if (currentDate.getMonthValue() >= 10 && currentDate.getMonthValue() <= 11) {
            tempD = RandomNumber(4000,4100);
        }else if (currentDate.getMonthValue() >11) {
            tempD = RandomNumber(4100,4200);
        }else if (currentDate.getMonthValue() <= 3) {
            tempD = RandomNumber(4200,4300);
        }else {
            tempD = RandomNumber(4000,4100);
        }
        tempD = Math.round(tempD * Math.pow(10, 3)) / Math.pow(10, 3);
        return String.valueOf(tempD);
    }
    public static String CosCh(LocalDate currentDate, String con) {
        double cons = Double.parseDouble(con)/2.5;
        double tempD = 0;
        if (currentDate.getMonthValue() >= 5 && currentDate.getMonthValue() < 10) {
            tempD = RandomNumberD(cons,cons+100);
        }else if (currentDate.getMonthValue() >= 10 && currentDate.getMonthValue() <= 11) {
            tempD = RandomNumberD(cons,cons+100);;
        }else if (currentDate.getMonthValue() >11) {
            tempD = RandomNumberD(cons,cons+100);;
        }else if (currentDate.getMonthValue() <= 3) {
            tempD = RandomNumberD(cons,cons+100);;
        }else {
            tempD = RandomNumberD(cons,cons+100);;
        }
        tempD = Math.round(tempD * Math.pow(10, 3)) / Math.pow(10, 3);
        return String.valueOf(tempD);
    }

    public String CosF(LocalDate currentDate,String con) {
        double cons = Double.parseDouble(con)/1.4;
        double tempD = 0;
        if (currentDate.getMonthValue() >= 5 && currentDate.getMonthValue() < 10) {
            tempD = RandomNumberD(cons,cons+100);
        }else if (currentDate.getMonthValue() >= 10 && currentDate.getMonthValue() <= 11) {
            tempD = RandomNumberD(cons,cons+100);;
        }else if (currentDate.getMonthValue() >11) {
            tempD = RandomNumberD(cons,cons+100);;
        }else if (currentDate.getMonthValue() <= 3) {
            tempD = RandomNumberD(cons,cons+100);;
        }else {
            tempD = RandomNumberD(cons,cons+100);;
        }
        tempD = Math.round(tempD * Math.pow(10, 3)) / Math.pow(10, 3);
        return String.valueOf(tempD);
    }

    public static double RandomNumber(int f, int l) {

        return (Math.random() *( l-f) )+f;

    }

    public static double RandomNumberD(double f, double l) {
        return (Math.random() * (l-f ))+f;
    }

}
