# https://particuliers.engie.fr/electricite/conseils-electricite/conseils-tarifs-electricite/consommation-electrique-moyenne-logement-par-superficie.html#:~:text=Pour%20calculer%20la%20consommation%20moyenne,650%20kWh%20(mauvaise%20isolation).
# https://www.service-public.fr/particuliers/vosdroits/F32563#:~:text=Dans%20un%20logement%20chauff%C3%A9%2C%20la,est%20pas%20consid%C3%A9r%C3%A9%20comme%20d%C3%A9faillant.

from datetime import datetime, timedelta
import csv
import random

# Incrémentation de la date
def incDate(start_date):
    incDate = start_date + timedelta(1)
    return incDate

num_rows = 1000
# Données à écrire
fields = ['Id', 'Date', 'Lieu', 'Temp', 'Chauffage', 'Tarif / kWh/jour']

# Lignes à insérer
Final_rows = []

# Date de depart
cur_date = datetime.strptime("01-01-2018", "%d-%m-%Y").date()

# Lieux où sont récoltées les données
lieu = ['Vannes', 'Lorient', 'Renne', 'Pontivy', 'Brest', 'Quimper']

# Id de depart
id= 0

for i in range(num_rows): # Boucle le nombre de jour(s) que l'on veut
    cur_date = incDate(cur_date)
    for j in range (len(lieu)): # Boucle sur le nombre de lieu(x) que l'on veut par jour
        id = id + 1
        cur_lieu = lieu[j]
        temp = str(random.uniform(17.0, 21.0)) # Données au hasard, selon des moyennes du gouvernement (voir le lien au dessu)
        chauffage = random.choice(['Actif', 'Eteint']) # Si le chauffage est éteint ou allumé
        if chauffage == 'Eteint': # Si le chauffage est eteint alors 0 consommation
            tarif = 0
        else: # Sinon le tarif est en moyenne 1.5 fois la température du foyer
            tarif = float(temp) * (random.uniform(1.34, 1.62))
        Final_rows.append([id,cur_date,cur_lieu,temp,chauffage,tarif])

# # Nom du fichier csv
filename = "Temp_gen.csv"

# Ecrire dans le fichier csv
with open(filename, 'w') as csvfile:
    csvwriter = csv.writer(csvfile)
    csvwriter.writerow(fields)
    for row in Final_rows:
        csvwriter.writerow(row)