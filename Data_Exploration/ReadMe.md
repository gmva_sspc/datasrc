# Dataset Exploration

Data which we researched during the project and have not been used in the end.

## Table of contents


- [Data_Generation](#data-generation)
- [Dataset_HVAC](#dataset-hvac)
- [Dataset_Regional](#dataset-regional)

## Data generation

* We created a [Python program](Data_Exploration/Data_Generation/Generation_data.py) which generates a csv file to test our database with data.

* We created another [Java program](Data_Exploration/Data_Generation/GenerationData2.java) that generates more data and more accurate.

After visualizing this data, we noticed that the randomly generated data were not really coherent and always had the same pattern.

## Dataset HVAC

Dataset we found and contains the types of data we wanted. 

* We created a [Telegraf plugin](Data_Exploration/Dataset_HVAC/example_pluginHVAC.conf) to insert it in InfluxDB.

* We tried to use it but as the time period was too small (1 week worth of data) we dropped it.

## Dataset regional

Dataset which we used to visualize data on InfluxDB but was lacking different types of data and thus was dropped.

* We created a [Telegraf plugin](Data_Exploration/Dataset_HVAC/pluginRegional.conf) to insert it in InfluxDB.

* We had to transform the date format in the csv to a date format accepted by Go.

This dataset comes from (https://odre.opendatasoft.com - 04/01/2023)

