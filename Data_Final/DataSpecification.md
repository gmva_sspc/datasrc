|Name|Data|Column name|Description|Explication|Number of data points|Unit|Sampling frequency|Missing rate of the raw data (2021 - 2023)|Specific available time period (default is 3 years from 2021 to 2023)|
|:----|:----|:----|:----|:----|:----|:----|:----|:----|:----|
|Heating|Energy|hvac_1|Heating Ventilation and Air Conditioning load for the first building.|Energy consumption of HVAC in the first building.|1|kW|15 min|0,08| |
| | |hvac_2|Heating Ventilation and Air Conditioning load for the second building.|Energy consumption of HVAC in the second building.|1|kW|15 min|0,08| |
| |Indoor environmental |cooling|Cooling temperature setpoint of Zone *|Setpoint which, when attained activate the cooling system.|41|°C|5 min|0.05-0.07|Sep 2021 -- Dec 2023|
| | |heating|Heating temperature setpoint of Zone *|Setpoint which, when attained activate the heating system.|41|°C|5 min|0.05-0.06| |
| | |temperature|Temperature of the corresponding zone|Temperature detected by the sensors in the corresponding zone.|51|°C|5 min|0.15-0.20| |
| |CO2|co2|CO2 concentration of the corresponding zone|CO2 concentration of the corresponding zone.|13|ppm|5 min|0-0.1|Aug -- Dec 2022, Apr -- Dec 2023|
| |RTU|supply_air_temperature_setpoint|Roof Top Unit * supply air temperature setpoint (*: 001, 002, 003, 004)|Setpoint which, when attained supplies air temperature to the RTU concerned.|4|°C|5 min|0,15| |
| | |supply_air_temperature|Roof Top Unit * supply air temperature (*: 001, 002, 003, 004)|Air temperature supplied by the RTU.|4|°C|5 min|0,14| |
| | |supply_air_flow|Roof Top Unit * filtered supply air flow rate (*: 001, 002, 003, 004)|Air flow of the supply filtered air|4|CFM = 1,7 m³/h|5 min|0,14| |
|Weather|Localisation |measurement|the name of the city| | |-|1 hour| |1 year from january 2022 to january 2023|
| | |country|the name of the country| | |-|1 hour| |1 year from january 2022 to january 2023|
| | |latitude|coordinate| | |-|1 hour| |1 year from january 2022 to january 2023|
| | |longitude|coordinate| | |-|1 hour| |1 year from january 2022 to january 2023|
| | |sunrise|hour of sunrise| | |-|1 hour| |1 year from january 2022 to january 2023|
| | |sunset|hour of sunset| | |-|1 hour| |1 year from january 2022 to january 2023|
| |Weather indicators |humidity|humidity in the air | | |%|1 hour| |1 year from january 2022 to january 2023|
| | |windDeg|direction of the wind| | | |1 hour| |1 year from january 2022 to january 2023|
| | |windSpeed|speed of the wind | | |m/s|1 hour| |1 year from january 2022 to january 2023|
| | |pressure|atmospheric pressure| | |hPa|1 hour| |1 year from january 2022 to january 2023|
| | |clouds|cloud presence| | |%|1 hour| |1 year from january 2022 to january 2023|
| | |rain|quantity of rain that has fallen| | |mm/m²/h|1 hour| |1 year from january 2022 to january 2023|
| | |snow|quantity of snow that has fallen| | |mm/m²/h|1 hour| |1 year from january 2022 to january 2023|
| | |weather|weather adjective| | |-|1 hour| |1 year from january 2022 to january 2023|
| | |weatherDesc|decription of the weater| | |-|1 hour| |1 year from january 2022 to january 2023|
| | |weatherIcon|icon of the API Open Weather Map| | |-|1 hour| |1 year from january 2022 to january 2023|
| |Temperature|temperature|temperature at the moment| | |°C|1 hour| |1 year from january 2022 to january 2023|
| | |temperatureMin|smallest temperature of the day| | |°C|1 hour| |1 year from january 2022 to january 2023|
| | |temperatureMax|highest temperature of the day| | |°C|1 hour| |1 year from january 2022 to january 2023|
| | |feelsLike|felt temperature| | |°C|1 hour| |1 year from january 2022 to january 2023|
