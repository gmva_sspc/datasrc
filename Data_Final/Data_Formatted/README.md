# DataSet Structure

Here is the structure of the dataset which we created after formatting the data.

```bash
└── dataset_final.zip
        ├── co2
        │   ├── batiment1.csv
        │   └── batiment2.csv
        ├── indoor
        │   ├── batiment1.csv
        │   └── batiment2.csv
        ├── electricite
        │   ├── batiment1.csv
        │   └── batiment2.csv
        └── RTU
            ├── batiment1.csv
            └── batiment2.csv

```