# Dataset Final

Dataset which we found during the phase of data exploration. We chose it and changed it to fit our needs.

## Table of contents

- [Data Original](#data-original)
- [Data Formatted](#data-formatted)
- [Script Formatting](#script-formatting)
- [Script Insertion](#script-insertion)

## Data Original

During the phase of data exploration, we selected this dataset as it corresponds the most to our needs for this project.
(This dataset comes from [this article](https://www.nature.com/articles/s41597-022-01257-x) - 05/04/2022).

This dataset was curated from an office building constructed in 2015 in Berkeley, California, which includes whole-building and end-use energy consumption, HVAC system operating conditions, indoor and outdoor environmental parameters.

## Data Formatted

This dataset has not been entirely used, only a part of it.
It's the version after being formatted by [Script_Formatting](#Script_Formatting) so the attributes correspond to our needs and to the InfluxDB format. We also changed the date to simulate data in 2023.
This dataset was inserted in InfluxDB and visualized with Grafana.

## Script Formatting

Script we created to transform the data in the format wanted by InfluxDB, [Format Script](/Data_Final/Script_Formatting/).
As some csv of the dataset were merged, the time period between data has been changed.

## Script Insertion

Script we created to insert the data in the buckets of the database, [Insert Script](/Data_Final/Script_Insertion/sendData.py).
A delay was added to ensure that the server wouldn't crash when inserting the dataset.