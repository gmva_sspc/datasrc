import pandas as pd

pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', None)

supply_air_temperature_setpoint= pd.read_csv('doi_10.7941_D1N33Q__v6-20230201T163340Z-001/doi_10.7941_D1N33Q__v6/Building_59/Bldg59_clean data/rtu_sa_t_sp.csv')
supply_air_temperature= pd.read_csv('doi_10.7941_D1N33Q__v6-20230201T163340Z-001/doi_10.7941_D1N33Q__v6/Building_59/Bldg59_clean data/rtu_sa_t.csv')
supply_air_flow = pd.read_csv('doi_10.7941_D1N33Q__v6-20230201T163340Z-001/doi_10.7941_D1N33Q__v6/Building_59/Bldg59_clean data/rtu_sa_fr.csv')

"""
Transform the data from wide to long
"""
supply_air_temperature_setpoint = pd.melt(supply_air_temperature_setpoint, id_vars=['date'])
supply_air_temperature = pd.melt(supply_air_temperature, id_vars=['date'])
supply_air_flow = pd.melt(supply_air_flow, id_vars=['date'])

"""
Rename the columns like:
date, zone, temperature
"""
supply_air_temperature_setpoint = supply_air_temperature_setpoint.rename(columns={'variable': 'RTU', 'value': 'supply_air_temperature_setpoint'})
supply_air_temperature = supply_air_temperature.rename(columns={'variable': 'RTU', 'value': 'supply_air_temperature'})
supply_air_flow = supply_air_flow.rename(columns={'variable': 'RTU', 'value': 'supply_air_flow'})


"""
Format the date and set it as index
"""
supply_air_temperature_setpoint['date'] = pd.to_datetime(supply_air_temperature_setpoint['date'], format="%Y-%m-%d %H:%M:%S")
supply_air_temperature_setpoint.set_index(['date'])

supply_air_temperature['date'] = pd.to_datetime(supply_air_temperature['date'], format="%Y-%m-%d %H:%M:%S")
supply_air_temperature.set_index(['date'])

supply_air_flow['date'] = pd.to_datetime(supply_air_flow['date'], format="%Y-%m-%d %H:%M:%S")
supply_air_flow.set_index(['date'])


"""
Merge the data
"""
result = pd.merge(supply_air_flow, supply_air_temperature, on=["date", "RTU"])
result = pd.merge(result,supply_air_temperature_setpoint , on=["date", "RTU"])
result.set_index(['date'])
result['supply_air_temperature_setpoint'] = result['supply_air_temperature_setpoint'].apply(lambda x: (x-32) * 5/9)
result['supply_air_temperature'] = result['supply_air_temperature'].apply(lambda x: (x-32) * 5/9)
result['supply_air_flow'] = result['supply_air_flow'].apply(lambda x: (x-32) * 5/9)
result["date"] = result["date"] + pd.Timedelta(days=365*3)


"""
To simulate the environment, we split the data in two parts (Batiment 1 and Batiment 2)
"""
index = result[result['RTU'] == 'RTU_003'].index[0] # take the index of the first sensor of the second building
batiment1 = result.iloc[:index] #take the data of the first building
batiment2 = result.iloc[index:] #take the data of the first building
batiment1.set_index(['date'])
batiment1.set_index(['date'])

"""
Save the data in csv file
"""
batiment1.to_csv("dataset_final/RTU/batiment1.csv",index=False)
batiment2.to_csv("dataset_final/RTU/batiment2.csv",index=False)