import pandas as pd

"""
read the data from the csv file
"""
d_co2 = pd.read_csv('doi_10.7941_D1N33Q__v6-20230201T163340Z-001/doi_10.7941_D1N33Q__v6/Building_59/Bldg59_clean data/zone_co2.csv')
"""
Transform the data from wide to long
"""

d_co2 = pd.melt(d_co2, id_vars=['date'])



"""
Rename the columns like:
date, sensor, temperature
"""
d_co2 = d_co2.rename(columns={'variable': 'sensor', 'value': 'co2'})

"""
Format the date and set it as index
"""
d_co2['date'] = pd.to_datetime(d_co2['date'], format="%Y-%m-%d %H:%M:%S")
d_co2.set_index(['date'])

"""
Transform
"""

d_co2["date"] = d_co2["date"] + pd.Timedelta(days=365*3)

"""
wqTo simulate the environment, we split the data in two parts (Batiment 1 and Batiment 2)
"""

nbsensor = len(d_co2['sensor'].unique())
nb_per_bat = int(nbsensor/2)
first_sensor_bat2 = d_co2['sensor'].unique()[nb_per_bat] #take the name of first sensor of the second building
index = d_co2[d_co2['sensor'] == first_sensor_bat2].index[0] # take the index of the first sensor of the second building

batiment1_co2 = d_co2.iloc[:index] #take the data of the first building
batiment2_co2 = d_co2.iloc[index:] #take the data of the second building

"""
Save the data in csv file
"""
batiment1_co2.to_csv("dataset_final/co2/batiment1.csv", index=False)
batiment2_co2.to_csv("dataset_final/co2/batiment2.csv", index=False)