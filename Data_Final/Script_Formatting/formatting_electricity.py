import re
import time
import pandas as pd
# from influxdb_client import InfluxDBClient, WriteOptions

# Set the options of panda
pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', None)

# Read the csv
power = pd.read_csv('doi_10.7941_D1N33Q__v6-20230201T163340Z-001/doi_10.7941_D1N33Q__v6/Building_59/Bldg59_clean data/ele.csv')

# Select which columns will be dropped : drop('NAME OF THE COLUMN TO DROP', inplace=True, axis=1)
power.drop('mels_S', inplace=True, axis=1)
power.drop('lig_S', inplace=True, axis=1)
power.drop('mels_N', inplace=True, axis=1)
power.drop('Unnamed: 6', inplace=True, axis=1)

power = power.rename(columns={'hvac_N': 'hvac_1', 'hvac_S': 'hvac_2'})

# Melt the data of the file
power = pd.melt(power, id_vars=['date'])

# Rename the columns choosed
power = power.rename(columns={'variable': 'hvac', 'value': 'Consumption'})

# Change the format of the date
power['date'] = pd.to_datetime(power['date'], format="%Y-%m-%d %H:%M:%S")

# Transform the date so the data is real-time
power["date"] = power["date"] + pd.Timedelta(days=365*3)

# Set the index to be the date
power.set_index(['date'])

print(power.head(10))

index = power[power['hvac'] == 'hvac_2'].index[0] # take the index of the first sensor of the second building

batiment1 = power.iloc[:index] # take the data of the first building
batiment2 = power.iloc[index:] # take the data of the second building

batiment1.to_csv("dataset_final/electricite/batiment1.csv", index=False)
batiment2.to_csv("dataset_final/electricite/batiment2.csv", index=False)