import pandas as pd

"""
read the data from the csv file
"""
d_indoor = pd.read_csv('doi_10.7941_D1N33Q__v6-20230201T163340Z-001/doi_10.7941_D1N33Q__v6/Building_59/Bldg59_clean data/zone_temp_exterior.csv')
d_cooling = pd.read_csv('doi_10.7941_D1N33Q__v6-20230201T163340Z-001/doi_10.7941_D1N33Q__v6/Building_59/Bldg59_clean data/zone_temp_sp_c.csv')
d_heating = pd.read_csv('doi_10.7941_D1N33Q__v6-20230201T163340Z-001/doi_10.7941_D1N33Q__v6/Building_59/Bldg59_clean data/zone_temp_sp_h.csv')
"""
Transform the data from wide to long
"""
d_indoor = pd.melt(d_indoor, id_vars=['date'])
d_cooling = pd.melt(d_cooling, id_vars=['date'])
d_heating = pd.melt(d_heating, id_vars=['date'])

"""
Rename the columns like:
date, sensor, temperature
"""
d_indoor = d_indoor.rename(columns={'variable': 'sensor', 'value': 'temperature'})
d_cooling = d_cooling.rename(columns={'variable': 'sensor', 'value': 'cooling'})
d_heating = d_heating.rename(columns={'variable': 'sensor', 'value': 'heating'})

"""
Format the date and set it as index
"""
d_indoor['date'] = pd.to_datetime(d_indoor['date'], format="%Y-%m-%d %H:%M:%S")
d_indoor.set_index(['date'])

d_cooling['date'] = pd.to_datetime(d_cooling['date'], format="%Y-%m-%d %H:%M:%S")
d_cooling.set_index(['date'])

d_heating['date'] = pd.to_datetime(d_heating['date'], format="%Y-%m-%d %H:%M:%S")
d_heating.set_index(['date'])

"""
Merge the data
"""
result = pd.merge(d_indoor, d_cooling, on=["date", "sensor"])
result = pd.merge(result, d_heating, on=["date", "sensor"])

"""
Convert the temperature from fahrenheit to celsius
"""
result['temperature'] = result['temperature'].apply(lambda x: (x-32) * 5/9)
result['cooling'] = result['cooling'].apply(lambda x: (x-32) * 5/9)
result['heating'] = result['heating'].apply(lambda x: (x-32) * 5/9)

"""
Transform
"""
result["date"] = result["date"] + pd.Timedelta(days=365*3)

"""
wqTo simulate the environment, we split the data in two parts (Batiment 1 and Batiment 2)
"""

nbsensor = len(result['sensor'].unique()) # take the number of sensor
nb_per_bat = int(nbsensor/2) # take the number of sensor in the first building
first_sensor_bat2 = result['sensor'].unique()[nb_per_bat] #take the name of first sensor of the second building
index = result[result['sensor'] == first_sensor_bat2].index[0] # take the index of the first sensor of the second building

batiment1 = result.iloc[:index] #take the data of the first building
batiment2 = result.iloc[index:] #take the data of the second building

"""
Save the data in csv file
"""
batiment1.to_csv("dataset_final/indoor/batiment1.csv", index=False)
batiment2.to_csv("dataset_final/indoor/batiment2.csv", index=False)
