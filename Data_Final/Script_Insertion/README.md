# User manual of the python script

We have created a python script which insert data in InfluxDB.

## Table of contents
-  [How to use the insertion script](#comment-utiliser-le-script-dinsertion)
-  [Format of input CSV file](#format-of-input-csv-file)
    - [Format of the date](#-le-format-de-la-date)
    - [Format of the CSV](#-le-format-du-csv)
-  [Test](#test)

## How to use the insertion script

The script [send_data.py](/Data_Final/Script_Insertion/sendData.py) make it easier to insert data in InfluxDB

Optional arguments: 

| Parameter                 | Description   |	
| :------------------------ | :-------------|
| `-h` or `--help` | show this help message and exit|

   Example:
   ```
   sendData.py -h
   ```
Required arguments:

| Parameters                 | Description   |	
| :------------------------ | :-------------|
| `-i` or `--input` | input data file|
| `-u` or `--url` | url of influxdb server
|`-o` or `--orga` | name of the organization|
| `-t` or `--token` | token|
| `-m` or `--measurement` | name of the measurement|
| `-b` or `--bucket` | name of the bucket in which we want to insert data|
| `-tag` or `--tag` | name of the column which contains the tag|
| `-time` or `--time` | name of the column which contains the date|

   Example of command:
   ```
   python .\sendData.py -i .\datafinale\electricite\batiment1.csv 
                         -o SAE-SSPC 
                         -u http://localhost:8086/ 
                         -t Xru84Az56m6FL7kUACUv3g4lGyisK5z5RbZPpHpU_cWjEgVxbQLUXpl3ckhSBfKv687TgRrNxvO7hf1iZU2j2w==
                         -m electricite
                         -b SSPC
                         -tag hvac 
                         -time date
   ```

## Format of input CSV file

There are some conditions to respect so that the insertion of data in the database is successful.

### - Format of the date

The dates must be in the form of "2020-12-31 00:00:00"
which corresponds to "year-month-day hour:minute:second"

### - Format of the CSV

The format of the csv is really important and must be like that :

- Excel view

| The date    | The sensor   | The data x  | The data y | The data z|
| :------- |:-------|:----|:------|:------|

- Text view
```
The data , the sensor ,  the data x , the data y , the data z
```

## Test

We did some tests to check if our scripts works.

- Excel view

|date|RTU|supply_air_flow|supply_air_temperature|supply_air_temperature_setpoint|
| :--- |:-----|:----|:------|:------|
|2020-12-31 00:00:00 | RTU_001 | 7833.02 | 19.77 | 20.0|
|2020-12-31 00:01:00 | RTU_001 | 7851.34 | 19.77|20.0|
|2020-12-31 00:02:00 | RTU_001 | 8261.56|19.72 | 20.0|
|2020-12-31 00:00:00 | RTU_002 | 7759.30 | 20.22 | 21.11|
|2020-12-31 00:01:00 | RTU_002 | 7796.25 | 20.38 | 21.11|
|2020-12-31 00:02:00 | RTU_002 | 8156.53 | 20.44 | 21.11|

- Text view
```
date,RTU,supply_air_flow,supply_air_temperature,supply_air_temperature_setpoint
2020-12-31 00:00:00 , RTU_001 , 7833.02 , 19.77 , 20.0
2020-12-31 00:01:00 , RTU_001 , 7851.34 , 19.77,20.0
2020-12-31 00:02:00 , RTU_001 , 8261.56,19.72 , 20.0
2020-12-31 00:00:00 , RTU_002 , 7759.30 , 20.22 , 21.11
2020-12-31 00:01:00 , RTU_002 , 7796.25 , 20.38 , 21.11
2020-12-31 00:02:00 , RTU_002 , 8156.53 , 20.44 , 21.11
```