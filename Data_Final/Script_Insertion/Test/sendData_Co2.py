from influxdb_client import InfluxDBClient
import pandas as pd
import time

URL = "https://sspc.fbyx.fr/"
ORGA = "SSPC"
TOKEN = "tokenHERE"
MeasurementName = "co2"
tag = "sensor"
timestamp = "date" # default value

def Insertion(path, BUCKET):
    data = pd.read_csv(path)
    i2 = 0
    i = 1000
    client = InfluxDBClient(url=URL,
                            token=TOKEN,
                            org=ORGA)

    for i in range(0, len(data), 1000):
        print("Nombre de données inserées ", i, " soit ", round(i / len(data) * 100, 2), "%") # with pourcentage

        df_cut = data.iloc[i2:i, :]
        with client.write_api() as write_client:
            write_client.write(bucket=BUCKET, record=df_cut, data_frame_measurement_name=MeasurementName,
                               data_frame_tag_columns=[tag], data_frame_timestamp_column=timestamp)
        i2 = i
        time.sleep(0.05) # limit 20 request per second

if __name__ == "__main__":
    Insertion("dataset_final/co2/batiment1.csv", "batiment_1")
    Insertion("dataset_final/co2/batiment2.csv", "batiment_2")


