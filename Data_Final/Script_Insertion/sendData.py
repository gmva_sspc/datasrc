import sys
from influxdb_client import InfluxDBClient
import pandas as pd
import time
import argparse

pathIn = ""
URL = ""
ORGA = ""
BUCKET = ""
TOKEN = ""
MeasurementName = ""
tag = ""
timestamp = "date"

def Insertion():
    data = pd.read_csv(pathIn)
    i2 = 0
    i = 1000
    client = InfluxDBClient(url=URL,
                            token=TOKEN,
                            org=ORGA)

    for i in range(0, len(data), 1000):
        print("Nombre de données inserées ", i, " soit ", round(i / len(data) * 100, 2), "%") # with pourcentage

        df_cut = data.iloc[i2:i, :]
        with client.write_api() as write_client:
            write_client.write(bucket=BUCKET, record=df_cut, data_frame_measurement_name=MeasurementName,
                               data_frame_tag_columns=[tag], data_frame_timestamp_column=timestamp)
        i2 = i
        time.sleep(0.05) # limit 20 request per second


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", help="input file", required=True)
    parser.add_argument("-u", "--url", help="url of influxdb server", required=True)
    parser.add_argument("-o", "--orga", help="name of the organization in which we want to insert data", required=True)
    parser.add_argument("-t", "--token", help="token", required=True)
    parser.add_argument("-m", "--measurement", help="measurement", required=True)
    parser.add_argument("-b", "--bucket", help="bucket", required=True)
    parser.add_argument("-tag", "--tag", help="tag", required=True)
    parser.add_argument("-time", "--time", help="time")
    args = parser.parse_args()

    if args.input:
        pathIn = args.input
    if args.url:
        URL = args.url
    if args.token:
        TOKEN = args.token
    if args.orga:
        ORGA = args.orga
    if args.measurement:
        MeasurementName = args.measurement
    if args.bucket:
        BUCKET = args.bucket
    if args.tag:
        tag = args.tag
    if args.time:
        timestamp = args.time

    Insertion()