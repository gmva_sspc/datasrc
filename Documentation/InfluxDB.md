# InfluxDB

## Introduction

**InfluxDB** est un SGBD permettant de mieux gérer les séries temporelles et de supporter des charges élevées d'écriture et de requête. Il peut être utilisé en tant que registre de sauvegarde pour n'importe quel cas d'utilisation nécessitant des données horodatées y compris la surveillance DevOps, les métriques d'applications, les capteurs de données de l'IoT et l'analyse en temps réel.

InfluxDB fait également parti du *TICK stack* qui correspond à une collection de technologies qui sont :  

1. Telegraf
2. InfluxDB
3. Chronograf
4. Kapacitator

Ensemble ces technologies offrent une plateforme qui peut capturer, surveiller, stocker et visualiser toutes les données dans une série chronologique permettant de prendre des décisions commerciales éclairées en temps réel.

## InfluxDB dans le detail 
InfluxDB est conçu pour gérer les bases de données du type TSDB (time series database), qui contient des séries temporelles. Ces bases de données sont employées notamment pour stocker et analyser des données de capteurs. Elle peut donc supporter des millions d’enregistrements entrants, comme nos instruments de mesure scientifiques en qui fournissent un flux continu de données. Elle assure une synchronisation de l'heure sur l'ensemble de ses appareils grâce au Network Time Protocol (NTP).
InfluxDB opère une distinction entre tags et fields concernant les colonnes. Alors qu’un tag contient seulement des métadonnées qui sont indexées, les fields contiennent des valeurs qui pourront être analysées par la suite. Grâce à cette méthode InfluxDB offrent des avantages évidents en matière de rapidité pour l’enregistrement et le traitement des données horodaté. Il peut également maintenir des vitesses d’écriture élevées pendant une durée prolongée du fait que l’index est très simple.





## Manuel d'installation (2.5)

- ### Windows

Configuration requise : 
* Windows 10 ou +
* 64-bit AMD architecture
* Un Powershell
1. Télécharger InfluxDB avec ce lien : 
>https://dl.influxdata.com/influxdb/releases/influxdb2-2.5.1-windows-amd64.zip
2. Dezipper l'archive dans le dossier souhaiter.
Dans notre cas ce sera Program : Files\InfluxData.
Renommer le dossier influxdb.
3. Ouvrer un terminal a l'emplacement du fichier : 
>`cd -Path 'C:\Program Files\InfluxData\influxdb'`<
4. Démarrer InfluxDB avec la commande : 
>`influxd` ou `.\influxd`
5. Pour Setup InfluxDB connectez vous a linterface web  https://localhost:8086/

Vous arriverez alors sur la page d'accueil.

![](Documentation/Image/InfluxDB/Welcome.png)

Cliquez sur **Get Started** afin de créer le premier utilisateur.

![](Documentation/Image/InfluxDB/GetStarted.png)

On doit renseigner son *nom d'utilisateur*, son *mot de passe*, le *nom de l'organisation* et le *nom du bucket*.

- ### Linux

Ubuntu 20.04

Pour commencer verifier que votre linux est a jour : 
>`sudo apt update`
>`sudo apt upgrade`
1. Pour installer le service InfluxDB utiliser les commandes suivantes :
>`wget -q https://repos.influxdata.com/influxdb.key`

>`echo '23a1c8836f0afc5ed24e0486339d7cc8f6790b83886c4c96995b88a061c5bb5d influxdb.key' | sha256sum -c && cat influxdb.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/influxdb.gpg > /dev/null`

>`echo 'deb [signed-by=/etc/apt/trusted.gpg.d/influxdb.gpg] https://repos.influxdata.com/debian stable main' | sudo tee /etc/apt/sources.list.d/influxdata.list`

>`sudo apt-get update && sudo apt-get install influxdb2`
2. Lancez influxd db grace a la commande  : 
>`influxd`

3. Pour configuration InfluxDB connectez vous a linterface web  https://localhost:8086/
Pour cette étape, vous pouvez vous référer à l'étape de Windows.


- ### Docker
Pour installer InfluxDB sur Docker, vous devez d'abord vous assurer que Docker est installé sur votre ordinateur. Si ce n'est pas le cas, vous pouvez suivre les instructions d'installation sur le site officiel de Docker.

Une fois Docker installé, vous pouvez utiliser la commande suivante pour télécharger l'image Docker d'InfluxDB à partir du référentiel Docker Hub :

> sudo docker pull influxdb

Cela téléchargera l'image Docker d'InfluxDB sur votre ordinateur. Une fois le téléchargement terminé, vous pouvez utiliser la commande suivante pour lancer un conteneur InfluxDB à partir de cette image :

> sudo docker run -p 8086:8086 -v /influxdb:/var/lib/influxdb influxdb

Cette commande lancera un conteneur InfluxDB en écoutant sur le port 8086 et en montant le répertoire /influxdb sur votre ordinateur comme répertoire de données pour InfluxDB. Cela vous permettra de conserver les données d'InfluxDB même si le conteneur est arrêté ou supprimé.

Pour executer le docker en arriere plan, il faut ajouter l'option `-d` après l'option `run`

Vous pouvez vérifier que InfluxDB est en cours d'exécution avec la commande suivante :

> sudo docker ps  -a

Pour finir l'installation d'InfluxDB sur Docker, voir le point 5 dans la partie pour Windows


## Manuel d'utilisation (version 2.5)

La commande permettant de lancer le serveur est :

>`service influxdb start`

Cela doit renvoyer 

>`$ influx -precision rfc3339`
`Connected to http://localhost:8086 version 1.8.10`
`InfluxDB shell 1.8.10`
`>`

Afin de changer les configurations par défaut vous pouvez exécuter la commande suivante :
>`influx --help`

Vous pouvez désormais exécuter des commandes InfluxQL.

Pour sortir du Shell InfluxQL, entrez :
>`exit`

Vous pouvez créer une base de donnée en exécutant la commande :
>`influx`
>`> CREATE DATABASE mydb`

Remplacer mydb par le nom que vous souhaitez donner à la base de donnée.

Vous pouvez voir les bases de données avec la requête : 
>`> SHOW DATABASES`

Vous pouvez dire clairement sur quelle base de donnée vous voulez executer la requête avec : 
>`> USE mydb`
>`Using database mydb`

Vous pouvez executer des commandes Sql classiques comme par exemple : 

>`> INSERT personne, nom=Jack, prenom=Rochard age=34`
>`> SELECT "nom", "prenom", "age" FROM "personne"`

Enfin, vous pouvez changer le format des réponses avec les commandes :
>`influx -format=csv`

Vous pouvez remplacer csv par le format json