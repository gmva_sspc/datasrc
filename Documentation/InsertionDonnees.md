# **Création d'un plugin avec un exemple de données**

## Sommaire : 
* Introduction
* Création du plugin
* Insertion des données
* Conclusion

## Introduction

Afin de tester InfluxDB / Telegraf, nous voulons insérer un jeu de données test.

Pour insérer un jeu de données, le format le plus courant est *CSV*.

Telegraf nous permets de faire cela:

<img src="Documentation/Image/InsertionDonnees/Upload.png" width="500"/>

Or Influxdb, utilise un format CSV spéficique :

<img src="Documentation/Image/InsertionDonnees/ErrorCSV.png" width="700"/>

Pour cela nous avons besoin de créer un plugin.


## Création du plugin

Pour insérer des données au format CSV classique, nous devons donc créer un plugin Telegraf afin de les formater pour les insérer dans notre base (bucket) Influxdb.

> PS: Pour la création d'un plugin voir le document [Telegraf.md](Telegraf.md)

<img src="Documentation/Image/InsertionDonnees/CreationPlugin.png" width="700"/>


La configuration du plugin que nous avons utilisé est la suivante : 

> [[inputs.file]]<br>
> files = ["CheminFichierCSV/Exemple.csv"]<br>
> data_format = "csv"<br>
> csv_header_row_count = 1<br>
> csv_delimiter = ","<br>
> csv_timestamp_column = "ID"<br>
> csv_timestamp_format = "2006-01-02T15:04:05"<br>

Selon le nom de la colonne contenant les dates il faut changer `csv_timestamp_column`. Dans notre cas, la columne se nomme `ID` 

En ce qui concerne le format des dates il faut changer `csv_timestamp_format`. 
On peut également changer la séparation des valeurs avec `csv_delimiter`.


## Insertion des données

Pour démarrer le plugin télégraf nous avons suivi les instructions indiquées : 

<img src="Documentation/Image/InsertionDonnees/TelegrafSetup.png" width="500"/>

Une fois que les données ont été upload dans InfluxDB on peut ensuite les observer via différents graphs dont on peut choisir les filtres.

<img src="Documentation/Image/InsertionDonnees/DataExplorer.png" width="700"/>

## Conclusion

En conclusion, nous avons vu qu'il est possible d'insérer un jeu de données au format CSV dans InfluxDB en utilisant Telegraf et en créant un plugin. 
Une fois que le plugin est configuré et que Telegraf est démarré, les données du fichier CSV sont insérées dans la base de données et nous pouvons les visualiser en utilisant différents graphs et en filtrant les données selon nos besoins.
