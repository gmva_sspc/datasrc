# Guide d'installation de PCWin2

PCwin2 est un logiciel développé par Sofrel, une entreprise française spécialisée dans les solutions de télégestion pour les réseaux d'eau, d'énergie et d'environnement. 
Ce logiciel est conçu pour être utilisé avec les équipements de télégestion de Sofrel.

PCwin2 permet de programmer, configurer, surveiller et gérer à distance les équipements de télégestion de Sofrel. 
Il offre une interface conviviale et intuitive pour les utilisateurs, avec des fonctionnalités avancées telles que la visualisation en temps réel des données de télégestion, la programmation d'alarmes et de scénarios de commande, et la génération de rapports.

### Etape 1

On a téléchargé un dossier fournit par le client qui permet d'installer leur application.

Afin d'installer PCWin2, il faut aller dans *'1_PCWiN2/1_Setup/'* et exécuter *'Package-PCWin 2-V4.50.34.exe'*.

### Etape 2

Suivez les instructions afin d'installer PCWin2 : 

![](https://i.imgur.com/W8H7W4r.png)

Pour la configuration SQL Server, choisissez le mode local :

![](https://i.imgur.com/9vKfn2a.png)

Ensuite, suivez les instructions et vous arriverez sur la phase d'authentification de PCWin2 : 

![](https://i.imgur.com/Ah8hoGR.png)

### Etape 3

Enfin, vous devez redémarrer votre ordinateur et PCWin2 sera installé.

![](https://i.imgur.com/c0P78cQ.png)

Vous pouvez également récupérer des configurations déjà existantes grâce à des .sav qu vous devrez mettre dans : *'C:\ProgramData\Lacroix Sofrel\PCWin 2\Saves'*
