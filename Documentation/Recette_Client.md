# Recette Client

| Tâche                                                                                                  | Description de la tâche                                                                                          | Statut |
| :----------------------------------------------------------------------------------------------------- | :--------------------------------------------------------------------------------------------------------------- | :----: |
| Récupérer des données brutes du client                                                                 | Récupérer les données des années précédentes du client                                                           |   ⌛   |
| Récupérer des données d'un flux continu du client                                                      | Récupérer en temps réel les données des capteurs du client                                                       |   ⌛   |
| Création d'un guide pour faciliter la gestion des erreurs potentielles et la compréhension des données | Créer un guide expliquant les données et les mesures pour une meilleure compréhension                            |   ✅   |
| Récupérer des données de dataset similaires aux données finales                                        | Récupérer des données similaires aux données finales pour commencer l'analyse                                    |   ✅   |
| Créer des données à partir de programmes ou de calculs                                                 | Créer manuellement des données à l'aide de programmes pour simuler les capteurs du client                        |   ✅   |
| Insérer les données dans une base de données commune à tous les groupes                                | Insérer les données dans la base de données InfluxDB commune à tous les groupes                                  |   ✅   |
| Rédaction d'un guide d'utilisation du convertisseur CSV → Influx                                       | Créer un guide expliquant le programme Python, ainsi que comment le modifier pour convertir d'autres données CSV |   ✅   |
| Création d'une documentation expliquant les données                                                    | Créer une documentation expliquant l'historique des données                                                      |   ✅   |
