# Telegraf

## Introduction

**Telegraf** est un agent serveur qui permet de collecter et de transmettre toutes les données. Il peut être exécuté sur n'importe quel système et n'a pas besoin de dépendance. Il contient également une mémoire tampon qui peut permettre de stocker des informations si la BDD est momentanément indisponible.
Il compte plus de 300 plugins créés par la communauté. Les plus intéressants dans notre cas seront les plugins d'entrée, de traitement / d'agrégation et de sortie. Dans le cas spécifique des capteurs, si aucun des 300 plugins ne nous convient nous pouvons créer le nôtre avec n'importe quel langage, il sera compatible avec Telegraf.
Les plugins les plus connu sont :
1. MQTT
2. SNMP
3. HTTP listener
4. CloudNatch


Cet outil presente un réel avantage car il nous permet de faire le lien entre nos données en sortie de capteur, de les traiter (création d'autres données à partir de differents calculs), de détecter les éventuelles erreurs ou disfonctionements des capteurs, et de les injecter dans la base InfluxDB.


## Manuel d'installation

- ## Ubuntu & Debian

1. Il faut installer Telegraf avec les commandes suivantes :-1: 
>wget -q https://repos.influxdata.com/influxdb.key

>echo '23a1c8836f0afc5ed24e0486339d7cc8f6790b83886c4c96995b88a061c5bb5d influxdb.key' | sha256sum -c && cat influxdb.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/influxdb.gpg > /dev/null

>echo 'deb [signed-by=/etc/apt/trusted.gpg.d/influxdb.gpg] https://repos.influxdata.com/debian stable main' | sudo tee /etc/apt/sources.list.d/influxdata.list

>sudo apt-get update && sudo apt-get install influxdb2


- ## Windows

1. Il faut installer Telegraf depuis le lien (https://portal.influxdata.com/downloads/) :
2. Extraire le .zip dans C:\Program Files\InfluxData\Telegraf



## Manuel d'utilisation 

- ## Ubuntu & Debian


Maintenant il vous faut lancer InfluxDB (voir le [doc](Documentation/InfluxDB.md) )
1. Choissisez Telegraf              
![](Documentation/Image/Telegraf/1.png)
2. Crée une nouvelle 
![](Documentation/Image/Telegraf/2.png)
3. Choissez le bucket et le plugin de votre choix 
![](Documentation/Image/Telegraf/3.png)
5. Remplire le nom de la config et la description
![](Documentation/Image/Telegraf/5.png)

#### Dans votre terminal: 
1. Exporter la clef API (API Token) en tant que variable d'environement. 
2. Lancer telegraf avec la commande donné si dessus.                  
![](Documentation/Image/Telegraf/6.png)


## Bravo votre Telegraf est Fonctionelle !



## Test des Plugins:

- ## Script 
1. Voici un exemple de scipte qui peut marcher !

> from(bucket: "Jowt_bucket")

>  |> range(start: v.timeRangeStart, stop: v.timeRangeStop)

>  |> filter(fn: (r) => r["_measurement"] == "cpu")

>  |> filter(fn: (r) => r["_field"] == "usage_user")

>  |> filter(fn: (r) => r["cpu"] == "cpu-total")

>  |> filter(fn: (r) => r["host"] == "jonathan-Nitro")

>  |> aggregateWindow(every: v.windowPeriod, fn: mean, createEmpty: false)

>  |> yield(name: "mean")


- ## Garphique 
1. Une fois arriver sur l'onglet graphique nous pouvons verifier que le plugin d'entrée selectioné marche bien. 
2. Pour cela nous choisissons le bon bucket, puis le bon plugin et enfin les parametres que l'on veut voir.
![](Documentation/Image/Telegraf/7.png)


