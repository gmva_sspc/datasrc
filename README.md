# Table des matières

- [Introduction](#introduction)
- [Schéma explicatif du projet](#schéma-explicatif-du-projet)
- [Vidéo explicatif du projet](#vidéo-explicatif-du-projet)
- [Outils de déploiement du projet SSPC](#Outils-de-déploiement-du-projet-SSPC)
  - [Partie Installation](Partie-Installation)
  - [Configuration](Configuration)
  - [Annexe](Annexe)
- [Data Client](#data-client)
- [Data Exploration](#data-exploration)
- [Data Final](#data-final)

# Introduction
Le patrimoine de Golfe du Morbihan Vannes Agglomération (GMVA) a
évolué ces dernières années à la fois en quantité mais aussi en
technicité. Le suivi et l’amélioration des performances énergétiques des
installations communautaires doit permettre de réduire les
consommations énergétiques et ainsi atteindre les objectifs collectifs
du Plan Climat Air Energie territorial, notamment de diminuer de 30%
les consommations du territoire à horizon 2030 par rapport à 2012.

L’objectif du projet proposé est d’analyser les données de suivi des
installations électriques, chauffage, ventilation, climatisation en
vue de l’amélioration de l’efficacité énergétique du patrimoine et le
suivi des installations de production d’énergie renouvelable de la
collectivité et ainsi proposer les modules de régulation et des
systèmes d’alerte pouvant être fonction, par exemple de données de
prévisions météorologiques, etc.

Dans ce projet, il s'agira donc:

* d’organiser le recueil de la donnée GTB / GTC sur les équipements
  les plus consommateurs de la collectivité;
* d’harmoniser la restitution des données;
* de stocker des données harmonisées dans une base de données.


# Schéma explicatif du projet

![](Documentation/Image/Readme/sspcExplication.png)

# Vidéo explicative du projet

![](/Documentation/Video_de_demonstration_SSPC.mp4)

# Outils de déploiement du projet SSPC

CE SCRIPT PERMET DE DEPLOYER LES PROJETS SSPC SUR DOCKER.
CETTE INSTALLATION EST DONC À FAIRE UNE SEULE FOIS SUR 1 DES 4 PROJETS.
Ce projet permet de déployer les projets SSPC sur docker.
Il est composé de 4 scripts bash permettant de télécharger, de construire et de lancer les conteneurs docker, ainsi qu'un fichier docker-compose afin de configurer les différents conteneurs.

## Partie Installation 
Il faut préalablement installer docker et docker-compose sur votre machine.

Pour tout faire il faut exécuter le fichier `all.sh` qui va d’abord télécharger les repos avec `download.sh` puis construire les images des projets avec `build.sh` et enfin lancer les conteneurs avec le docker-compose grâce à `run.sh`.

Donc pour commencer il faut exécuter la commande suivante dans le dossier du projet :

`sudo sh ./all.sh`

## Configuration

Il faut configurer InfluxDB comme expliqué dans la documentation du projet (le pdf).

Il faut aussi configurer un compte Gmail pour les rapports de chauffage. (Voir le tutoriel du groupe chauffage)

Une fois la configuration d'InfluxDB faite, vous devez avoir un nom d'organisation, un nom de bucket ainsi qu'un AllAccessToken que vous avez créé.

Il faut ensuite modifier le fichier `docker-compose.yml` et remplacer les valeurs des variables d'environnement du projet chauffage par les valeurs que vous avez récupérer.

Voici les valeurs à remplacer :
```
environment:
    - MAIL=YOUR MAIL HERE !!!
    - MAIL_SENDER=YOUR MAIL SENDER HERE !!!
    - MAIL_PASSWORD=YOUR MAIL PASSWORD HERE !!!
    - TOKEN_INFLUXDB=YOUR TOKEN HERE !!!
    - ORGANIZATION_INFLUXDB=YOUR ORGANIZATION HERE !!!
```

Après les avoir remplacer, il faut ré-exécuter le fichier all.sh pour prendre en compte les modifications.

Après ça, il reste quelques configuration à faire sur grafana pour le groupe de visualisation ainsi que la configuration des tokens du projet méteo_prev.

## Annexe

Voici les adresse web des différents projets après le déploiement :

- [Groupe Web : http://localhost:80](http://localhost:80)
- [Meteo Prev : http://localhost:83](http://localhost:83)
- [InfluxDB : http://localhost:81](http://localhost:81)
- [Grafana : http://localhost:82](http://localhost:82)

(le localhost peut être remplacer par l'adresse IP de la machine)


# Data Client

Création de scripts en python pour récupérer les données des clients par email, les formater et les insérer dans InfluxDB, afin que l'équipe de visualisation de données puisse les utiliser.

# Data Exploration

Les données qui ont été trouvé au cours de nos recherches et qui n'ont pas été utilisé dans le cadre de la visualisation ou dans l'application web car soit elles étaient sur période de temps trop courte, soit elles n'avaients pas assez d'attributs.

# Data Final

Les données que nous avons trouvé et qui ont été utilisé dans le cadre de la visualisation et dans l'application web.
Ces données ont été modifié afin de correspondre à nos besoins.